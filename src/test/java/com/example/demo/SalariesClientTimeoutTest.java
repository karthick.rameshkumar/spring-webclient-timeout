package com.example.demo;

import io.netty.channel.ChannelOption;
import io.netty.channel.epoll.EpollChannelOption;
import io.netty.handler.timeout.ReadTimeoutException;
import io.netty.handler.timeout.ReadTimeoutHandler;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientRequestException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;
import reactor.netty.resources.ConnectionProvider;
import reactor.test.StepVerifier;

import static org.mockserver.integration.ClientAndServer.startClientAndServer;

import java.net.URI;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class SalariesClientTimeoutTest {

    private static final String BASE_URL = "http://localhost:1080";

    private ClientAndServer mockServer;

    private static final String DUMMY_RESPONSE = "DUMMY_RESPONSE";

    private static final int DEFAULT_API_LATENCY_IN_MS = 5000;

    private WebClient noConfigWebclient;

    private WebClient customWebClient;

    @Before
    public void startMockServer() {

        mockServer = startClientAndServer(1080);

        mockServer.when(HttpRequest
                        .request()
                        .withMethod("GET")
                        .withPath("/randomSalariesEndpoint")
                ).
                respond(HttpResponse
                        .response()
                        .withBody("{ \"result\": \"ok\"}")
                        .withDelay(TimeUnit.MILLISECONDS, DEFAULT_API_LATENCY_IN_MS)
                );

        noConfigWebclient = WebClient.builder().build();

        final HttpClient httpClient = HttpClient
                .create(ConnectionProvider.create("default"))
                .tcpConfiguration(client ->
                                client
                                        .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 1)
                                        .option(EpollChannelOption.TCP_KEEPIDLE, 5)
                                        .option(EpollChannelOption.TCP_KEEPINTVL, 5)
                                        .option(EpollChannelOption.TCP_KEEPCNT, 10)
                                        .doOnConnected(conn -> conn.addHandlerLast(new ReadTimeoutHandler(4)))
                                );

        customWebClient = WebClient
                .builder()
                .clientConnector(new ReactorClientHttpConnector(httpClient))
                .build();

    }

    private Mono<String> callSalaries(URI uri, long signalTimeout) {
        return noConfigWebclient
                .get()
                .uri(uri)
                .retrieve()
                .onStatus(HttpStatus::isError, r -> {
                    System.out.println("Status code " + r.statusCode());
                    throw new RuntimeException("Error while calling  randomSalariesEndpoint");
                })
                .bodyToMono(String.class)
                .timeout(Duration.ofMillis(signalTimeout))
                .onErrorReturn(DUMMY_RESPONSE);
    }

    private Mono<String> callSalaries1(URI uri, long timeout) {

        return customWebClient
                .get()
                .uri(uri)
                .retrieve()
                .onStatus(HttpStatus::isError, r -> {
                    System.out.println("Status code " + r.statusCode());
                    throw new RuntimeException("Error while calling  randomSalariesEndpoint");
                }).bodyToMono(String.class)
                .timeout(Duration.ofMillis(timeout))
                .doOnError(t -> System.out.println(t.toString()));
    }


    private Mono<String> callSalaries2(URI uri) {

        return customWebClient
                .get()
                .uri(uri)
                .retrieve()
                .onStatus(HttpStatus::isError, r -> {
                    System.out.println("Status code " + r.statusCode());
                    throw new RuntimeException("Error while calling  randomSalariesEndpoint");
                })
                .bodyToMono(String.class)
                .onErrorReturn(DUMMY_RESPONSE);
    }



    @Test
    public void testSalariesEndpoint_timeoutLessThanAPILatency() {

        /*
         * API latency = 5s
         * Signal timeout = 3s
         * Expected: DUMMY_RESPONSE
         */

        URI uri = UriComponentsBuilder
                .fromUriString(BASE_URL + "/randomSalariesEndpoint")
                .build()
                .toUri();

        Mono<String> result = callSalaries(uri, 3000);

        StepVerifier
                .create(result)
                .expectSubscription()
                .assertNext(r -> Assert.assertEquals(DUMMY_RESPONSE, r))
                .verifyComplete();
    }

    @Test
    public void testSalariesEndpoint_throwsWebClientRequestException() {

        /*
         * API latency = 5s
         * Signal timeout = 15s
         * Expected: WebClientRequestException(ReadTimeoutException)
         */

        URI uri = UriComponentsBuilder
                .fromUriString(BASE_URL + "/randomSalariesEndpoint")
                .build()
                .toUri();

        Mono<String> result = callSalaries1(uri, 15000);

        StepVerifier
                .create(result)
                .expectSubscription()
                .expectError(WebClientRequestException.class)
                .verify();
    }

    @Test
    public void testSalariesEndpoint_returnsDummyObject() {

        /*
         * API latency = 5s (default)
         * Signal timeout = Not configured
         * Expected: DUMMY_OBJECT
         */

        URI uri = UriComponentsBuilder
                .fromUriString(BASE_URL + "/randomSalariesEndpoint")
                .build()
                .toUri();

        Mono<String> result = callSalaries2(uri);

        StepVerifier
                .create(result)
                .expectSubscription()
                .assertNext(r -> Assert.assertEquals(DUMMY_RESPONSE, r))
                .verifyComplete();
    }

    @After
    public void stopMockServer() {
        mockServer.stop();
    }
}
